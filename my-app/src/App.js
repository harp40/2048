import React from 'react';
import {useKeyPress} from "./Move";
import './App.css'

function App() {
    const [board, setBoard] = React.useState([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])
    const [score, setScore] = React.useState(0);
    const up = useKeyPress('w');
    const down = useKeyPress('s');
    const left = useKeyPress('a');
    const right = useKeyPress('d');

    // const pressLeft = React.useCallback(()=> {
    //     let cell = 0;
    //     board.map( (row, index) => {
    //         row.map( cell => {
    //             row[0] = row[0]+row[1]+row[2]+row[3]
    //         })
    //     })
    // }, [board])

    React.useEffect( () => {
        (up || down || left || right) &&  setScore(score+1)

    }, [up,down,left, right])

  return (
    <div className="App">
        <div className='score'>
            <label>Счет: </label>
            {score}
        </div>
        <div className='board'>
            {board.map( row => {
                return(
                    <>
                        {row.map(cell => {
                            return(<div className='cell'>{cell}</div>)
                                })}
                    </>
                )})}
        </div>
        {up && 'up'}
        {down && 'down'}
        {left && 'left'}
        {right && 'right'}
    </div>
  );
}

export default App;
